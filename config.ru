require 'rack/cors'

#map '/log' do
#  use Rack::Auth::Basic, "Restricted Area" do |username, password|
#    [username, password] == ['admin', 'abc123']
#  end
#end

use Rack::Cors do
  allow do
    origins '*'
    resource '*', headers: :any, methods: [:get, :post, :options, :put]
  end
end
require ::File.expand_path('./../app.rb', __FILE__)
Ramaze.start(:file => __FILE__, :started => true)
run Ramaze