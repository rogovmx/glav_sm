require 'open-uri'
require 'net/smtp'
require "net/https"
require "uri"
require "json"
require 'ramaze'
#require 'pony'
require 'rack/cors'
#require 'rest-client'
require 'rubygems'
require 'httpclient'
require 'rubyntlm'
#require 'ntlm/http'
#require 'httpi-ntlm'
require 'active_support/core_ext/numeric/time'

Dir["./lib/*.rb"].each {|file| require file }


class Send < Ramaze::Controller
  
  map '/sm'
  def index
    params = request.POST
    name = params['name']  
    phone = params['phone'].to_s.strip
#    time = params['time']  
    domain = params['domain']  
    referrer = params['referrer']  
    if referrer.to_s.strip != ""
      ref = referrer.index('?') ? referrer[0..referrer.index('?')-1] : referrer
      ref = ref[0..-2] if ref[-1] == '/' 
    end
  raise ArgumentError, 'Argument is too long' if name.to_s.size > 100 || phone.to_s.size > 30 #|| time.to_s.size > 100 
  raise ArgumentError, 'No phone' if phone.to_s.strip == ""

    utm =  params['utm'][params['utm'].index('?')+1..-1] if params['utm'].index('?')
    utm = Hash[utm.split('&').map! { |i| i.split('=')}] unless utm.to_s.strip == ""
    utm ||= {}
    req = "https://docs.glavstroy.ru/crm_glavstroy/hs/Callback/v1/Get?"
    req += "PhoneNumber=#{phone.scan(/\d/).join('')}"
    req += "&referer=#{ref}" if ref
    req += "&site=#{domain}" unless domain.to_s.empty?
    req += "&created=#{(Time.now + 3.hours).to_i}"
    #req += "&desiredtime=#{time}" 
    req += "&utm_source=#{utm['utm_source']}"  unless utm['utm_source'].to_s.empty?
    req += "&utm_campaign=#{utm['utm_campaign']}"  unless utm['utm_campaign'].to_s.empty?
    req += "&utm_medium=#{utm['utm_medium']}"  unless utm['utm_medium'].to_s.empty?
#    req += "&utm_term=#{utm['utm_term']}"  unless utm['utm_term'].to_s.empty?
#    test = "https://docs.glavstroy.ru/crm_glavstroy/hs/Callback/v1/Get?PhoneNumber=79110366332&referer=refererdata&site=mysite.ru&created=201600506"

      domain = req
      user = 'web-to-crm2'
      password = 'kUqHrpAGE4'
      client = HTTPClient.new
      client.set_auth(nil,user,password)
      resp = client.get(domain).status
      
  rescue => error
    File.open("log.txt", 'a') { |file| file.puts "#{Time.now + 3.hours} #{request.ip} ERROR >> #{error}" }   
  ensure  
    File.open("log.txt", 'a') do |file| 
      file.puts "#{Time.now + 3.hours} #{request.ip} from site >> #{params}"
      file.puts "request: #{req}"
      file.puts "response: #{resp}"
    end
    return nil
  end
      
end

class ShowLog < Ramaze::Controller

  map '/log'
  
  def index
    txt = ''
    data = File.open("log.txt")
    until data.eof()
      txt << data.readline + "<br/>"
    end
    return txt
#    return IO.read("public/log.txt")
  end
  
end


